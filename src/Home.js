import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';

import ImageGallery from './components/MyImageGallery';
import './App.css';
import { ResponsiveContainer, PieChart, Pie, Legend } from 'recharts';
import LineChart from './components/MyBubble';

import HorizontalBar from './components/MyHorizontalBar';
import RadarChart from './components/MyDoughnut';
import Calendar from './components/MyCalendar';
import axios from "axios";




class Home extends Component {

    static jsfiddleUrl = '//jsfiddle.net/alidingling/6okmehja/';

    state={
        FollowersList :[],
      }
    
      componentWillMount(){
        this.fetchFollowers();
    }
    
    fetchFollowers(){
        axios.get(`http://localhost:3001/artist/followers`)
        .then(({ data }) => {
          console.log("data", data);
          this.setState({FollowersList: data});
        })
    }

    
    render() {

        
    const {FollowersList}=this.state;
    console.log("follow", FollowersList);

        return (
            <Container>
                <div className="fondApp">

                    <Row>
                        <Col md="6">
                            <Calendar />
                        </Col>
                        <Col md="6">
                            <ImageGallery  />
                        </Col>

                    </Row>
                    <Row>
                        <Col md="6">
                            <div style={{ width: '100%', height: 300 }}>
                                <ResponsiveContainer>
                                    <PieChart>
                                        < Pie data={FollowersList} dataKey="followers" nameKey="name"  fill="#8884d8" label  />
                                        <Legend />
                                    </PieChart>
                                </ResponsiveContainer>
                            </div>
                        </Col>
                        <Col md="6">
                            <RadarChart/>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="6">

                            <HorizontalBar />
                        </Col>
                        <Col md="6">

                        <LineChart/>
                        </Col>
                    </Row>
                </div>
            </Container>
        );
    }
}

export default Home;
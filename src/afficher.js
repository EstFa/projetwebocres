import React, { Component } from 'react'

import axios from 'axios';


class Affichage extends Component {

    state = {
        artist: []
      }
    
      componentDidMount() {
        axios.get(`http://localhost:3001/artists`)
          .then(res => {
            const artist = res.data;
            this.setState({ artist });
          })
      }

  render() {


    return (
      <div>
      <ul>
        { this.state.artist.map(artist => <li>{artist.name}</li>)}
       
      </ul>
      </div>
    )
  }
}

export default Affichage;
import React, { Component } from 'react'
import ImageGallery from 'react-image-gallery';
import "../../node_modules/react-image-gallery/styles/css/image-gallery.css";
import axios from 'axios';

class MyImageGallery extends Component {


  
  state={
    PicList :[],
  }

  componentWillMount(){
    this.fetchPics();
}

fetchPics(){
    axios.get(`http://localhost:3001/album/cover`)
    .then(({ data }) => {
      console.log("data", data);
      this.setState({PicList: data});
    })
}


  render() {

    const {PicList}=this.state;
    console.log("follow", PicList);
    return (
      <div>
        <ImageGallery items={PicList} />
      </div>
    )
  }
}

export default MyImageGallery;
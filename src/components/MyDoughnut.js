import React, { Component } from 'react'
import { Radar, RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis } from 'recharts';
import axios from "axios";


class MyDoughnut extends Component {

  static jsfiddleUrl = 'https://jsfiddle.net/alidingling/6ebcxbx4/';

  
  state={
    ListenList :[],
  }

  componentWillMount(){
    this.fetchListen();
}

fetchListen(){
    axios.get(`http://localhost:3001/artist/followers`)
    .then(({ data }) => {
      console.log("data", data);
      this.setState({ListenList: data});
    })
}

  render() {

    const {ListenList}=this.state;
    console.log("follow", ListenList);
    
    return (
      <div>
        <RadarChart cx={300} cy={250} outerRadius={150} width={500} height={500} data={ListenList}>
        <PolarGrid />
        <PolarAngleAxis dataKey="album" />
        <PolarRadiusAxis />
        <Radar name="Mike" dataKey="listenings" stroke="#8884d8" fill="#8884d8" fillOpacity={0.6} />
      </RadarChart>
      </div>
    )
  }
}

export default MyDoughnut;
import React, { PureComponent } from 'react';

import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';

import axios from "axios";


 class MyBubble extends PureComponent {

  static jsfiddleUrl = 'https://jsfiddle.net/alidingling/xqjtetw0/';



  state = {
    LikeList: [],
  }

  componentWillMount() {
    this.fetchLikes();
  }

  fetchLikes() {
    axios.get(`http://localhost:3001/track/likes`)
      .then(({ data }) => {
        console.log("genre", data);
        this.setState({ LikeList: data });
      })
  }

  render() {

    const {LikeList}=this.state;

    return (
      <LineChart
      width={500}
      height={300}
      data={LikeList}
      margin={{
        top: 5, right: 30, left: 20, bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend />
      <Line type="monotone" dataKey="likes" stroke="#8884d8" activeDot={{ r: 8 }} />
      
    </LineChart>
    );
  }
}




export default MyBubble;
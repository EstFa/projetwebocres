import React, { Component } from 'react'
import { BarChart, CartesianGrid, XAxis,YAxis, Tooltip, Legend, Bar } from 'recharts';

import axios from "axios";





class MyHorizontalBar extends Component {
  static jsfiddleUrl = 'https://jsfiddle.net/alidingling/30763kr7/';



  state = {
    AlbumList: [],
  }

  componentWillMount() {
    this.fetchAlbum();
  }

  fetchAlbum() {
    axios.get(`http://localhost:3001/artist/album`)
      .then(({ data }) => {
        console.log("data", data);
        this.setState({ AlbumList: data });
      })
  }


  render() {

    const {AlbumList}=this.state;
    return (
      <div>

        <BarChart width={500} height={250} data={AlbumList}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="album" fill="#82ca9d" />
        </BarChart>
      </div>
    )
  }
}

export default MyHorizontalBar;
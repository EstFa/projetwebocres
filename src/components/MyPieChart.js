import React, { Component } from 'react'
import { PieChart, Pie } from 'recharts';
import axios from "axios";






class MyPieChart extends Component {
  static jsfiddleUrl = '//jsfiddle.net/alidingling/6okmehja/';



  state={
    FollowersList :[],
  }

  componentWillMount(){
    this.fetchFollowers();
}

fetchFollowers(){
    axios.get(`http://localhost:3001/artist/followers`)
    .then(({ data }) => {
      console.log("data", data);
      this.setState({FollowersList: data});
    })
}


  render() {

    const {FollowersList}=this.state;
    console.log("follow", FollowersList);

    return (
      <div>
        <PieChart>
        <Pie data={FollowersList} dataKey="followers" nameKey="name" cx="50%" cy="50%" outerRadius={50} fill="#8884d8" />
        </PieChart>
      </div>
    )
  }
}

export default MyPieChart;
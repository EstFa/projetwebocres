import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Admin from './Admin';
import Home from './Home';

export default () => (
	<Switch>
		<Route path="/" exact component={Home} />
		<Route path="/admin" exact component={Admin} />
		
	</Switch>
);
import React, { Component } from 'react';
import { Button } from 'reactstrap';

import "../node_modules/react-image-gallery/styles/css/image-gallery.css";

import './App.css';

import { Link } from 'react-router-dom';
import Route from './Route';


class App extends Component {

  


  render() {
    return (

      <div className="App">
        <header className="App-header">

          <h1>Mon Dashboard</h1>


          <nav>
            
              <Button><Link to="/">Home</Link></Button>
              <Button><Link to="/admin">Admin</Link></Button>
           
          </nav>
        </header>
        <Route />
        <footer>
          Estelle Favreau et Guillaume de La Chapelle
        </footer>
      </div>

    );
  }
}

export default App;

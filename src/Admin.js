import React, { Component } from 'react';
import axios from "axios";
import Affichage from './afficher';

class Admin extends Component {
    state = {
        name: '',
      }
    
      handleChange1 = event => {
        this.setState({ name: event.target.value });
      }

      handleChange2 = event => {
        this.setState({ birth: event.target.value });
      }

      handleChange3 = event => {
        this.setState({ followers: event.target.value });
      }

      handleChange4 = event => {
        this.setState({ album: event.target.value });
      }
    
      handleSubmit = event => {
        event.preventDefault();
    
    
    
        axios.put('http://localhost:3001/artist', { 
            name: this.state.name,
            birth: this.state.birth,
            followers: this.state.followers,
            album: this.state.album
        })
          .then(res => {
            console.log(res);
            console.log(res.data);
          })
      }

render() {
    return (
        <div >
            <form onSubmit={this.handleSubmit}>
                <br></br>
                <label>
                    Nom:
                         <input type="text" name="name" onChange={this.handleChange1}/>
                </label>
                <br></br>
                <label>
                    Birth:
                         <input type="text" name="birth" onChange={this.handleChange2}/>
                </label>
                <br></br>
                <label>
                    Followers:
                         <input type="text" name="followers" onChange={this.handleChange3} />
                </label>
                <br></br>
                <label>
                    Album:
                         <input type="text" name="album"  onChange={this.handleChange4}/>
                </label>
                <br></br>
                <input type="submit" value="Submit" />
            </form>

            <Affichage/>

        </div>
    );
}
}

export default Admin;